﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    /// <summary>
    /// Model representation of a user currently login to the system.
    /// </summary>
    public class WebUserModel
    {
        /// <summary>
        /// Gets or sets the ID of the website user.
        /// </summary>
        public int ID
        {
            get;
            set;
        }
        
    }
}